import java.util.ArrayList;

import models.Country;
import models.Region;

public class App {
    public static void main(String[] args) throws Exception {
        // Khởi tạo danh sách các vùng của Việt Nam
        ArrayList<Region> regionsOfVietnam = new ArrayList<>();
        regionsOfVietnam.add(new Region("HN", "Hà Nội"));
        regionsOfVietnam.add(new Region("HCM", "Hồ Chí Minh"));
        regionsOfVietnam.add(new Region("DN", "Đà Nẵng"));

        // Khởi tạo danh sách các quốc gia
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(new Country("VN", "Việt Nam", regionsOfVietnam));
        countries.add(new Country("US", "United States", new ArrayList<Region>())); // Khởi tạo danh sách rỗng cho Mỹ
        countries.add(new Country("AUS", "Australia", new ArrayList<Region>()));
        countries.add(new Country("CA", "Canada", new ArrayList<Region>()));

        // In danh sách các quốc gia ra màn hình
        System.out.println("Danh sách các quốc gia:");
        for (Country country : countries) {
            System.out.println(country.getCountryName());
        }

        // In danh sách các vùng của Việt Nam ra màn hình
        for (Country country : countries) {
            if (country.getCountryCode().equals("VN")) {
                System.out.println("Các vùng của Việt Nam:");
                for (Region region : country.getRegions()) {
                    System.out.println(region.getRegionName());
                }
                break;
            }
        }
    }
}
